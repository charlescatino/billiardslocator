exports.definition = {
	config: {
		columns: {
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "cues": "double",
		    "tables": "double",
		    "talc": "boolean",
		    "description": "string",
		    "overall": "double",
		    "title": "string",
		    "place_id": "int"
		},
		adapter: {
			type: "sql",
			collection_name: "ratings",
			idAttribute: "id"
		}
	},		
	extendModel: function(Model) {		
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});
		
		return Model;
	},
	extendCollection: function(Collection) {		
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});
		
		return Collection;
	}
}

