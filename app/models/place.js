exports.definition = {
	config: {
		columns: {
			"id": "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "address": "string",
		    "lat": "double",
		    "long": "double",
		    "name": "string",
		    "type_id": "int"
		},
		adapter: {
			type: "sql",
			collection_name: "places",
			idAttribute: "id"
		}
	},		
	extendModel: function(Model) {		
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});
		
		return Model;
	},
	extendCollection: function(Collection) {		
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});
		
		return Collection;
	}
}