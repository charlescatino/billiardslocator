function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "#fff",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    var __alloyId0 = [];
    $.__views.view1 = Ti.UI.createView({
        id: "view1",
        backgroundColor: "#123"
    });
    __alloyId0.push($.__views.view1);
    $.__views.locationsList = Ti.UI.createTableView({
        id: "locationsList"
    });
    __alloyId0.push($.__views.locationsList);
    $.__views.view2 = Ti.UI.createView({
        id: "view2",
        backgroundColor: "#246"
    });
    __alloyId0.push($.__views.view2);
    $.__views.view3 = Ti.UI.createView({
        id: "view3",
        backgroundColor: "#48b"
    });
    __alloyId0.push($.__views.view3);
    $.__views.scrollableView = Ti.UI.createScrollableView({
        views: __alloyId0,
        id: "scrollableView",
        showPagingControl: "true"
    });
    $.__views.index.add($.__views.scrollableView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var tableData = [ {
        title: "Apples"
    }, {
        title: "Bananas"
    }, {
        title: "Carrots"
    }, {
        title: "Potatoes"
    }, {
        title: "Apples"
    }, {
        title: "Bananas"
    }, {
        title: "Carrots"
    }, {
        title: "Potatoes"
    }, {
        title: "Apples"
    }, {
        title: "Bananas"
    }, {
        title: "Carrots"
    }, {
        title: "Potatoes"
    }, {
        title: "Apples"
    }, {
        title: "Bananas"
    }, {
        title: "Carrots"
    }, {
        title: "Potatoes"
    }, {
        title: "Apples"
    }, {
        title: "Bananas"
    }, {
        title: "Carrots"
    }, {
        title: "Potatoes"
    }, {
        title: "Apples"
    }, {
        title: "Bananas"
    }, {
        title: "Carrots"
    }, {
        title: "Potatoes"
    }, {
        title: "Apples"
    }, {
        title: "Bananas"
    }, {
        title: "Carrots"
    }, {
        title: "Potatoes"
    }, {
        title: "Apples"
    }, {
        title: "Bananas"
    }, {
        title: "Carrots"
    }, {
        title: "Potatoes"
    } ];
    $.locationsList.setData(tableData);
    $.index.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;